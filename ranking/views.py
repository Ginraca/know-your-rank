from django.shortcuts import redirect, render
from django.core.paginator import Paginator, EmptyPage
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, JSON

# Create your views here.
def searchWorld(system, year):
  ranking= []
  sparql = SPARQLWrapper('http://localhost:3030/university_ranking/query')
  sparql.setQuery('''
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX ex: <http://example.org/>

    SELECT ?universityName ?rank ?year
    WHERE {{
      ?university rdf:type dbo:University;
                  ex:name ?universityName;
                  ex:{0}_{1}_world_rank ?rank.
      ex:{2}_{3}_world_rank ex:year ?year.
    }}
    ORDER BY ASC (?rank)
  '''.format(year, system, year, system)
  )
  sparql.setReturnFormat(JSON)
  qres = sparql.query().convert()
  qres = qres['results']['bindings']
  for row in qres:
    datas = {}
    for key in row:
      datas[key] = row[key]['value']
    ranking.append(datas)
  return ranking

def searchCountry(system, year, country):
  ranking= []
  sparql = SPARQLWrapper('http://localhost:3030/university_ranking/query')
  sparql.setQuery('''
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX ex: <http://example.org/>

    SELECT ?universityName ?rank ?year ?country
    WHERE {{
      ?university rdf:type dbo:University;
                  ex:name ?universityName;
                  ex:country ?country;
                  ex:{0}_{1}_world_rank ?rank.
      ex:{2}_{3}_world_rank ex:year ?year.
    FILTER (?country = "{4}")
    }}
    ORDER BY ASC (?rank)
  '''.format(year, system, year, system, country)
  )
  sparql.setReturnFormat(JSON)
  qres = sparql.query().convert()
  qres = qres['results']['bindings']
  for row in qres:
    datas = {}
    for key in row:
      datas[key] = row[key]['value']
    ranking.append(datas)
  return ranking

def world_ranking_list(request):
  year = request.GET.get('year')
  if year == None:
    year = 2012
  ranking = searchWorld('cwur', year)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'world_cwur.html', {'ranking': page, 'cwurActive': 'active'})

def world_times_list(request):
  year = request.GET.get('year')
  if year == None:
    year = 2011
  ranking = searchWorld('times', year)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'world_times.html', {'ranking': page, 'timesActive': 'active'})

def world_shanghai_list(request):
  year = request.GET.get('year')
  if year == None:
    year = 2005
  ranking = searchWorld('shanghai', year)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'world_shanghai.html', {'ranking': page, 'shanghaiActive': 'active'})

def univ_details(request):
  univ = ""
  if request.method == 'POST':
    univ = request.POST.get('univ')
    special = ['of', 'and', 'in', 'for', 'on', 'de']
    univ = univ.lower().split(' ')
    univ = ' '.join([word.capitalize() if word not in special else word for word in univ])
    sparql = SPARQLWrapper('http://localhost:3030/university_ranking/query')
    sparql.setQuery('''
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX ex: <http://example.org/>

      SELECT ?universityName ?cwur ?times ?shanghai ?country
      WHERE {{
        ?university rdf:type dbo:University;
                    ex:name "{0}";
                    ex:name ?universityName;
                    ex:country ?country.
        OPTIONAL {{ ?university ex:2015_cwur_world_rank ?cwur }} .
        OPTIONAL {{ ?university ex:2016_times_world_rank ?times }}  .
        OPTIONAL {{ ?university ex:2015_shanghai_world_rank ?shanghai }} .
      }}
    '''.format(univ)
    )
    sparql.setReturnFormat(JSON)
    qres = sparql.query().convert()
    qres = qres['results']['bindings']
    local = []
    for row in qres:
      datas = {}
      for key in row:
        datas[key] = row[key]['value']
      local.append(datas)
    
    sparql = SPARQLWrapper('https://dbpedia.org/sparql')
    sparql.setQuery('''
      SELECT ?univ ?abstract ?city ?mascot ?motto ?students ?link ?cityLink
      WHERE {{
        ?univ rdf:type dbo:University;
              dbp:name "{0}"@en.
        OPTIONAL {{ ?univ dbo:abstract ?abstract }}.
        OPTIONAL {{ ?univ dbo:city ?cityLink }}.
        OPTIONAL {{ ?univ dbo:motto ?motto }}.
        OPTIONAL {{ ?univ dbp:students ?students }}.
        OPTIONAL {{ ?univ foaf:isPrimaryTopicOf ?link }}.
        OPTIONAL {{ ?univ dbo:mascot ?mascot }}.
        OPTIONAL {{ ?cityLink rdfs:label ?city }} .
        FILTER (lang(?abstract) = 'en' and lang(?city)='en')
      }}
    '''.format(univ)
    )
    sparql.setReturnFormat(JSON)
    qres = sparql.query().convert()
    qres = qres['results']['bindings']
    dbpedia = []
    for row in qres:
      datas = {}
      for key in row:
        datas[key] = row[key]['value']
      dbpedia.append(datas)
    warn = ""
    if len(local) <=0 or len(dbpedia) <= 0:
      warn = "Tidak ada Data universitas"
    if warn != "":
      return render(request, 'univ_details.html', {'warn': warn})
  return render(request, 'univ_details.html', {'local': local[0], 'dbpedia': dbpedia[0]})

def country_ranking_list(request):
  year = request.GET.get('year')
  country = request.GET.get('country')
  special = ['of', 'and', 'in', 'for', 'on', 'de']
  country = country.lower().split(' ')
  country = ' '.join([word.capitalize() if word not in special else word for word in country])
  if year == None:
    year = 2012
  if country == None or country == "United States of America" or country == 'Usa':
    country = 'USA'
  ranking = searchCountry('cwur', year, country)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'country_cwur.html', {'ranking': page, 'country': country, 'cwurActive': 'active'})

def country_times_list(request):
  year = request.GET.get('year')
  country = request.GET.get('country')
  if year == None:
    year = 2011
  if country == None or country == "United States of America" or country == 'Usa':
    country = 'USA'
  ranking = searchCountry('times', year, country)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'country_times.html', {'ranking': page, 'country': country, 'timesActive': 'active'})

def country_shanghai_list(request):
  year = request.GET.get('year')
  country = request.GET.get('country')
  if year == None:
    year = 2005
  if country == None or country == "United States of America" or country == 'Usa':
    country = 'USA'
  ranking = searchCountry('shanghai', year, country)
  p = Paginator(ranking, 10)
  page_num = request.GET.get('page', 1)
  try:
    page = p.page(page_num)
  except EmptyPage:
    page = p.page(1)
  return render(request, 'country_shanghai.html', {'ranking': page, 'country': country, 'shanghaiActive': 'active'})