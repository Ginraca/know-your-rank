from . import views
from django.urls import path

app_name = 'ranking'

urlpatterns = [
  path('world/', views.world_ranking_list, name='world'),
  path('world-times/', views.world_times_list, name='world_times'),
  path('world-shang/', views.world_shanghai_list, name='world_shanghai'),
  path('university/', views.univ_details, name='university'),
  path('country/', views.country_ranking_list, name='country'),
  path('country-times/', views.country_times_list, name='country_times'),
  path('country-shang/', views.country_shanghai_list, name='country_shanghai'),
]
