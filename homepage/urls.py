from . import views
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

app_name = 'homepage'

urlpatterns = [
  path('', views.homepage),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
