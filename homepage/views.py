from django.shortcuts import render
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, JSON, N3
from pprint import pprint

# Create your views here.
# def searchLocal():
#   datas = {}
#   sparql = SPARQLWrapper('http://localhost:3030/coba/sparql')
#   sparql.setQuery('''
#     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#     PREFIX owl: <http://www.w3.org/2002/07/owl#>
#     PREFIX ns1: <http://schema.org/>
#     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
#     PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
#     PREFIX foaf: <http://xmlns.com/foaf/0.1/>
#     SELECT ?nama ?orang
#     WHERE {
#       ?orang a foaf:Person;
#         ns1:name ?nama.
#     }
#   '''
#   )
#   sparql.setReturnFormat(JSON)
#   qres = sparql.query().convert()
#   qres = qres['results']['bindings']
#   # print(qres)
#   new_data = []
#   for row in qres:
#     new_row = {}
#     for key in row:
#       new_row[key] = row[key]['value']
#     new_data.append(new_row)
#   print(new_data)
  # return datas

def homepage(request):
  return render(request, 'home.html')